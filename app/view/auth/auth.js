$(document).ready(function () {

  $('.username, .password').keypress(function(event) {
    const keyCode = (event.keyCode ? event.keyCode : event.which);
    if (keyCode == 13) { $('.btn-sign-in').click(); return false; }    
  });

  $('.btn-sign-in').click(function() {
    const username = $('.username').val();
    const password = $('.password').val();

    if (!username || !password) {
      alert('Username or Password is empty');
    } else {
      $.ajax({
        method: 'POST',
        type: 'JSON',
        url: 'http://localhost:3000/api/usergame/login',
        data: { 'userName': username, 'password': password },
        success: function(resp) {
          console.log('resp', resp);
          localStorage.setItem('authUserGame', JSON.stringify(resp.data));
          $(location).attr('href','../dashboard/dashboard.html');
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(JSON.parse(xhr.responseText).message);
        }
      })
    }
  });

  $('.fullname, .username-regis, .password-regis').keypress(function(event) {
    const keyCode = (event.keyCode ? event.keyCode : event.which);
    if (keyCode == 13) { $('.btn-sign-up').click(); return false; }
  });

  $('.btn-sign-up').click(function() {
    const fullname = $('.fullname').val();
    const username = $('.username-regis').val();
    const password = $('.password-regis').val();

    if (!fullname || !username || !password) {
      alert('You must fill in all fields!');
    } else {
      $.ajax({
        method: 'POST',
        type: 'JSON',
        url: 'http://localhost:3000/api/usergame/create',
        data: {'fullName': fullname , 'userName': username, 'password': password }, //, 'year': year
        success: function(data) {
          console.log('data', data);
          alert(`Success Create User with username: ${data.userName} and fullname: ${data.fullName}`)
          $(location).attr('href','login.html');
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(JSON.parse(xhr.responseText).message);
        }
      })
    }
  });


})