$(document).ready(function () {

  let getAuthUser = localStorage.getItem('authUserGame');
  if (!getAuthUser) { alert('You Must Login First!'); $(location).attr('href','../auth/login.html'); }
  else {

    $.fn.getAllBiodata = function(keyword = null) {
      const url = keyword ? 'http://localhost:3000/api/ugbiodata/?keyword=' + keyword 
        : 'http://localhost:3000/api/ugbiodata/';

      $.ajax({
        method: 'GET',
        type: 'JSON',
        url: url,
        beforeSend: function () {
          $('.loading-container').show();
        },
        success: function(data) {
  
          let htmlBiodata = '';
          if (!data.length) {
            htmlBiodata += `<tr>`
            htmlBiodata += `  <td colspan="4" class="text-center">No Data Found.</td>`
            htmlBiodata += `</tr>`
          } else {
            $.each(data, function (idx, obj) {
              htmlBiodata += `<tr>`
              htmlBiodata += `  <th scope="row">${idx + 1}</th>`
              htmlBiodata += `  <td>${obj.gameName}</td>`
              htmlBiodata += `  <td>${obj.gameType}</td>`
              htmlBiodata += `  <td class="text-center">`
              htmlBiodata += `    <button class="btn btn-outline-warning show-modal-edit" value=${obj.id}>
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                    </svg>
                                  </button>&nbsp;`
              htmlBiodata += `    <button class="btn btn-outline-danger show-modal-delete" value=${obj.id}>
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                    </svg>
                                  </button>`
              htmlBiodata += `  </td>`
              htmlBiodata += `</tr>`
            });
          }
  
          $("#section-usergame-biodata").html(htmlBiodata);
         
          $('.show-modal-edit').click(function(e) {
            $('#modal-form').modal('show');
            $('.modal-title').text('Edit Game');
            $('.confirm-action').text('Edit');

            const id = $(this).attr("value");
            const getDataRow = data.find(x => x.id === parseInt(id));

            $('#input-game-id').val(getDataRow.id);
            $('#input-game-name').val(getDataRow.gameName);
            $('#input-game-type').val(getDataRow.gameType);
          });
  
          $('.show-modal-delete').click(function(e) {
            const id = $(this).attr("value");
            const getDataRow = data.find(x => x.id === parseInt(id));

            if (confirm(`Are you sure you want to delete ${getDataRow.gameName} in this list ?`)) {
  
              $.ajax({
                method: 'DELETE',
                type: 'JSON',
                url: 'http://localhost:3000/api/ugbiodata/'+id,
                data: { 'userId': getAuthUser.userId, 'gameName': getDataRow.gameName },
                beforeSend: function () {
                  $('.loading-container').show();
                },
                success: function(data) {
                  $('.toast').toast('show');
                  $('.toast-body').text('Delete Successfully');
                  $('.toast').css('background','red');

                  $.fn.getAllBiodata();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(JSON.parse(xhr.responseText).message);
                },
                complete: function () {
                  setTimeout(() => {
                    $('.loading-container').fadeOut(1000)
                  }, 500);
                }
              });
            }
          });
  
        },
        complete: function () {
          setTimeout(() => {
            $('.loading-container').fadeOut(1000)
          }, 500);
        }
      });
    }

    getAuthUser = JSON.parse(getAuthUser);
    $('.txt-fullname').text(`Welcome, ${getAuthUser.fullName}`);
    $.fn.getAllBiodata();
  }

  $('.show-modal-add').click(function() {
    $('#modal-form').modal('show');
    $('.modal-title').text('Add Game');
    $('.confirm-action').text('Save');

    $('#input-game-id').val('');
    $('#input-game-name').val('');
    $('#input-game-type').val('');
  });

  $('.btn-delete-all').click(function() {

    if (confirm(`Are you sure you want to delete all in this list ?`)) {
      $.ajax({
        method: 'DELETE',
        type: 'JSON',
        url: 'http://localhost:3000/api/ugbiodata/',
        data: { 'userId': getAuthUser.userId },
        beforeSend: function () {
          $('.loading-container').show();
        },
        success: function(data) {
          $('.toast').toast('show');
          $('.toast-body').text('Delete All Successfully');
          $('.toast').css('background','red');

          $.fn.getAllBiodata();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(JSON.parse(xhr.responseText).message);
        },
        complete: function () {
          setTimeout(() => {
            $('.loading-container').fadeOut(1000)
          }, 500);
        }
      });
    }
  });

  $('.input-search').keypress(function(event) {
    const keyCode = (event.keyCode ? event.keyCode : event.which);
    if (keyCode == 13) { $('.btn-search').click(); return false; }    
  });

  $('.btn-search').click(function() {
    const getSearch = $('.input-search').val();
    $.fn.getAllBiodata(getSearch);
  });

  $('.btn-history').click(function() {
    $('#modal-history').modal('show');
    $('.modal-title').text('User Game History');

    $.ajax({
      method: 'GET',
      type: 'JSON',
      url: 'http://localhost:3000/api/ugHistory',
      beforeSend: function () {
        $('.loading-container').show();
      },
      success: function(data) {
        let htmlHistory = '';

        if (!data.length) {
          htmlHistory += `<tr>`
          htmlHistory += `  <td colspan="5" class="text-center">No Data Found.</td>`
          htmlHistory += `</tr>`
        } else {
          $.each(data, function (idx, obj) {
            htmlHistory += `<tr>`
            htmlHistory += `  <th scope="row">${idx + 1}</th>`
            htmlHistory += `  <td>${obj.fullName}</td>`
            htmlHistory += `  <td>${obj.user_game_history ? obj.user_game_history.activity : 'no activity'}</td>`
            htmlHistory += `  <td>${obj.user_game_history ? new Date(obj.user_game_history.createdAt) : '-'}</td>`
            htmlHistory += `  <td>${obj.user_game_history ? new Date(obj.user_game_history.updatedAt) : '-'}</td>`
            htmlHistory += `</tr>`
          });
        }

        $("#section-usergame-history").html(htmlHistory);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(JSON.parse(xhr.responseText).message);
      },
      complete: function () {
        setTimeout(() => {
          $('.loading-container').fadeOut(1000)
        }, 500);
      }
    });

  });

  $('.btn-logout').click(function() {
    localStorage.removeItem('authUserGame');
    $(location).attr('href','../auth/login.html');
  });

  $('.confirm-action').click(function() {

    const getModeAction = $(this).text();
    const id       = $('#input-game-id').val();
    const gameName = $('#input-game-name').val();
    const gameType = $('#input-game-type').val();

    if (getModeAction === 'Save') {
      $.ajax({
        method: 'POST',
        type: 'JSON',
        url: 'http://localhost:3000/api/ugbiodata',
        data: { 'gameName': gameName, 'gameType': gameType, 'userId': getAuthUser.userId },
        beforeSend: function () {
          $('.loading-container').show();
        },
        success: function(data) {
          $('.toast').toast('show');
          $('.toast-body').text('Add Successfully');
          $('.toast').css('background','green');
          
          $('#modal-form').modal('toggle');
          $.fn.getAllBiodata();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(JSON.parse(xhr.responseText).message);
        },
        complete: function () {
          setTimeout(() => {
            $('.loading-container').fadeOut(1000)
          }, 500);
        }
      });
    } else {
      
      $.ajax({
        method: 'PUT',
        type: 'JSON',
        url: 'http://localhost:3000/api/ugbiodata/'+id,
        data: { 'gameName': gameName, 'gameType': gameType, 'userId': getAuthUser.userId },
        beforeSend: function () {
          $('.loading-container').show();
        },
        success: function(data) {
          $('.toast').toast('show');
          $('.toast-body').text('Edit Successfully');
          $('.toast').css('background','yellow');

          $('#modal-form').modal('toggle');
          $.fn.getAllBiodata();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(JSON.parse(xhr.responseText).message);
        },
        complete: function () {
          setTimeout(() => {
            $('.loading-container').fadeOut(1000)
          }, 500);
        }
      });
    }
  });

});