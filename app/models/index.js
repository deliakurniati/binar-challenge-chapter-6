const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.usergame = require("./usergame.model.js")(sequelize, Sequelize);
db.usergamebiodata = require("./usergame-biodata.model.js")(sequelize, Sequelize);
db.usergamehistory = require("./usergame-history.model.js")(sequelize, Sequelize);

db.usergame.hasOne(db.usergamehistory, {
  foreignKey: 'userId'
});

module.exports = db;
