module.exports = (sequelize, Sequelize) => {
  const UserGameHistory = sequelize.define("user_game_history", {
    userId: {
      type: Sequelize.INTEGER
    },
    activity: {
      type: Sequelize.STRING
    }
  });

  return UserGameHistory;
};
