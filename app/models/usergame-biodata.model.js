module.exports = (sequelize, Sequelize) => {
  const UserGameBiodata = sequelize.define("user_game_biodata", {
    gameName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    gameType: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });

  return UserGameBiodata;
};
