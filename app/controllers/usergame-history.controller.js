const db       = require("../models");
const Usergame = db.usergame;
const History  = db.usergamehistory;
const Op       = db.Sequelize.Op;


// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {

    Usergame.findAll({
      // attributes: ['fullName', 'user_game_history.activity'],
      include:[
        { model: History, required: false }
      ]
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};
