const db = require("../models");
const UsergameBiodata = db.usergamebiodata;
const UsergameHistory = db.usergamehistory;
const Usergame = db.usergame;
const Op = db.Sequelize.Op;

// Create and Save a new Usergame Biodata
exports.create = (req, res) => {

  // Validate request
  if (!req.body.gameName || !req.body.gameType) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a User Game Biodata
  const userGameBiodata = {
    gameName: req.body.gameName,
    gameType: req.body.gameType,
  };

  // Save User Game Biodata in the database
  UsergameBiodata.create(userGameBiodata)
    .then(async data => {
      const getUser = await Usergame.findOne({
        where: { userId: req.body.userId  },
        raw: true
      });

      // Send to history user game
      const userGameHistory = {
        userId: req.body.userId,
        activity: `${getUser.fullName} create game with name is ${userGameBiodata.gameName} and type game is ${userGameBiodata.gameType}`,
      };
      UsergameHistory.create(userGameHistory);

      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Usergame Biodata."
      });
    });
};

// Retrieve all Usergame Biodata from the database.
exports.findAll = (req, res) => {
  const keyword = req.query.keyword;
  var condition = keyword ? 
  {
    [Op.or]: [
      { gameName: { [Op.iLike]: `%${keyword}%` } },
      { gameType: { [Op.iLike]: `%${keyword}%` } }
    ]
  } 
  : null;

  UsergameBiodata.findAll({ where: condition, order: [ ['updatedAt',  'DESC'] ] })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Usergame Biodata."
      });
    });
};

// Find a single Usergame Biodata with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  UsergameBiodata.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Usergame Biodata with id=" + id
      });
    });
};

// Update a Usergame Biodata by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  UsergameBiodata.update(req.body, {
    where: { id: id }
  })
    .then(async num => {
      if (num == 1) {
        const getUser = await Usergame.findOne({
          where: { userId: req.body.userId  },
          raw: true
        });
  
        // Send to history user game
        const userGameHistory = {
          userId: req.body.userId,
          activity: `${getUser.fullName} Change game to ${req.body.gameName}`,
        };
        UsergameHistory.create(userGameHistory);

        res.send({ message: "Usergame Biodata was updated successfully." });

      } else {
        res.send({
          message: `Cannot update Usergame Biodata with id=${id}. Maybe Usergame Biodata was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Usergame Biodata with id=" + id
      });
    });
};

// Delete a Usergame Biodata with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  UsergameBiodata.destroy({
    where: { id: id }
  })
    .then(async num => {
      if (num == 1) {

        const getUser = await Usergame.findOne({
          where: { userId: req.body.userId  },
          raw: true
        });
  
        // Send to history user game
        const userGameHistory = {
          userId: req.body.userId,
          activity: `${req.body.gameName} just deleted by ${getUser.fullName}`,
        };
        UsergameHistory.create(userGameHistory);

        res.send({
          message: "Usergame Biodata was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Usergame Biodata with id=${id}. Maybe Usergame Biodata was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Usergame Biodata with id=" + id
      });
    });
};

// Delete all Usergame Biodata from the database.
exports.deleteAll = (req, res) => {
  UsergameBiodata.destroy({
    where: {},
    truncate: false
  })
    .then(async nums => {

      const getUser = await Usergame.findOne({
        where: { userId: req.body.userId  },
        raw: true
      });

      // Send to history user game
      const userGameHistory = {
        userId: req.body.userId,
        activity: `${getUser.fullName} already delete all biodata game`,
      };
      UsergameHistory.create(userGameHistory);

      res.send({ message: `${nums} Usergame Biodata were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Usergame Biodata."
      });
    });
};
