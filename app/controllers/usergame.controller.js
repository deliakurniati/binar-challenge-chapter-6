const db = require("../models");
const Usergame = db.usergame;
const Op = db.Sequelize.Op;

exports.login = (req, res) => {
  if (!req.body.userName || !req.body.password) {
    res.status(400).send({
      errorCode: 400,
      message: "Username or password is empty, please recheck again your field"
    });
    return;
  } else {
    Usergame.findOne({
      attributes: ['fullName', 'userName', 'userId'],
      where: {
        [Op.and]: [
          { userName: req.body.userName },
          { password: req.body.password }
        ]
      }
    }).then(data => {
      console.log('getLogin', data);
      if (data) {
        res.send({message: 'Login Success', data: data});
      } else {
        res.status(400).send({
          errorCode: 400,
          message: "Wrong username or password"
        });
      }
    });
  }

};

// Create and Save a new usergame
exports.create = (req, res) => {

  if (!req.body.fullName || !req.body.userName || !req.body.password) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  } else {
    Usergame.findOne({
      where: {
        userName: req.body.userName,
      }
    }).then(data => {
      console.log('getUsernameInDb', data);
      if (data) {
        res.status(400).send({
          message: "You cannot create the same of username"
        });
        return;
      } else {
        // Create a usergame
        const usergame = {
          fullName: req.body.fullName,
          userName: req.body.userName,
          password: req.body.password
        };

        // Save usergame in the database
        Usergame.create(usergame).then(data => {
          res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the usergame."
          });
        });
      }
    }).catch(err => {});
  }

};

// Find a single usergame with an id
exports.findOne = (req, res) => {
  const userId = req.params.userId;

  Usergame.findOne({
    where: {
      userId: userId,
    }
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving user data with id=" + userId
      });
    });
};
