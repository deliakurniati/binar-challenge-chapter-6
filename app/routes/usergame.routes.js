module.exports = app => {
  const usergame = require("../controllers/usergame.controller.js");

  var router = require("express").Router();

  // Create a new User game
  router.post("/create", usergame.create);

  router.post("/login", usergame.login);

  // Retrieve a single Tutorial with id
  router.get("/:userId", usergame.findOne);

  app.use("/api/usergame", router);
};
