module.exports = app => {
  const ugBiodata = require("../controllers/usergame-biodata.controller.js");

  var router = require("express").Router();

  // Create a new Usergame Biodata
  router.post("/", ugBiodata.create);

  // Retrieve all Usergame Biodata
  router.get("/", ugBiodata.findAll);

  // Retrieve a single Usergame Biodata with id
  router.get("/:id", ugBiodata.findOne);

  // Update a Usergame Biodata with id
  router.put("/:id", ugBiodata.update);

  // Delete a Usergame Biodata with id
  router.delete("/:id", ugBiodata.delete);

  // Create a new Usergame Biodata
  router.delete("/", ugBiodata.deleteAll);

  app.use("/api/ugbiodata", router);
};
