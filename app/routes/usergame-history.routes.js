module.exports = app => {
  const ugHistory = require("../controllers/usergame-history.controller.js");

  var router = require("express").Router();

   // Retrieve all Usergame History
   router.get("/", ugHistory.findAll);

  app.use("/api/ugHistory", router);
};
