const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
db.sequelize.sync();

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to User Game application." });
});

/** Internal Server Error Handler */
/** 505 Handler */
app.use(function (err, req, res, next) {
	console.error(err);
	res.status(500).json({
		status: 500,
		errors: 'Something when wrong'
	})
})

/** 404 Handler */
app.use(function (err, req, res, next) {
	res.status(404).json({
		status: 404,
		errors: 'Page not found'
	})
})

require("./app/routes/usergame.routes")(app);
require("./app/routes/usergame-biodata.routes")(app);
require("./app/routes/usergame-history.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});